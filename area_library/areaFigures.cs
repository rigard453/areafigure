namespace areaFigures;
public class area_Figures
{
    /// <summary>
    /// Метод вычисляет площадь круга
    /// </summary>
    /// <param name="r"> Радиус окружности</param>
    /// <returns></returns>
    public double culcAreaOfCircle (double r)
    {
        if (r < 0)
        {
            return 0;
        }
        return Math.PI * r * r;
    }
    /// <summary>
    /// Метод вычисления площади треугольника по 3 сторонам
    /// </summary>
    /// <param name="a"> сторона а</param>
    /// <param name="b"> сторона b</param>
    /// <param name="c"> сторона с</param>
    /// <returns></returns>
    public double culcAreaOfTriangle(double a, double b, double c)
    {
        double p = (a + b + c) / 2;
        if (a <= 0 || b <= 0 || c <= 0)
        {
            Console.WriteLine($"Данные введены некорректно!");
            return -1;
        }
        double[] str = { a, b, c };
        Array.Sort(str);
        if ((str[0]* str[0] + str[1] * str[1]) == (str[2] * str[2]))
        {
            Console.WriteLine($"Данный треугольник прямоугольный!");
        }

        if ((a < b + c) && (b < a + c) && (c < a + b))
        {
            return Math.Sqrt(p * (p - a) * (p - b) * (p - c));
        }
        else
        {
            Console.WriteLine($"Треугольник не существует.");
            return -1;
        }

        return 0;
    }

    /// <summary>
    /// Метод используется в случае незнания какая фигура будет вычисляться
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="c"></param>
    public void culcAreaOfFreeFigure(double a, double b = -10000, double c = -10000)
    {
        if ((a > 0) && (b == -10000) && (c == -10000))
        {
            culcAreaOfCircle(a);
        }
        if ((a > 0) && (b > -10000) && (c > -10000))
        {
            culcAreaOfTriangle(a, b, c);
        }
    }
}

